## **PROYECTO DE REACT AUX-SIS313**

**Created by: Univ. Lised Ariane Salazar Huanaco**

## 🧑‍💼**DIGITAL AGENCY** 👩‍💼

### ⏩**LINK TEMPLATE FIGMA**

[DIGITAL AGENCY](https://www.figma.com/file/f5bQAuKYy2EqwAWPUspfUu/Digital-Agency-(Community)?type=design&node-id=1-2&mode=design&t=jletm05OoBaKt0jy-0)

### ⏩**LINK DEL REPOSITORIO PRINCIPAL**
[MY REPOSITORIO](https://gitlab.com/salazarhuanacolisedariane/aux-sis313g1-i24)

### ⏩**AVANCES DEL TEMPLATE**
- [x]  [Header]()
- [x]  [Section 1]()
- [x]  [Footer]()

### ⏩**LINK PRPOYECTO-REACT**
[DIGITAL AGENCY](https://proyectoreactsis313.netlify.app/)
