export function Members(props:{image:any, nombre:string, ocupacion:string}){
    return(
        <div className="members"> 
            <img src={props.image} alt="" />
            <h3>{props.nombre}</h3>
            <p>{props.ocupacion}</p>
        </div>
    )
}