import Link from 'next/link'
import Marcas from './Marcas'

export function Header(){
    return (
        <div className="nav-header">
            <ul>
                <li>
                    <Link href="/" className='active'>HOME</Link>
                </li>
                <li>
                    <Link href="/">PAGES</Link>
                </li>
                <li>
                    <Link href="/">BLOG</Link>
                </li>
                <li>
                    <Link href="/">PORTFOLIO</Link>
                </li>
                <li>
                    <Link href="/">SHOP</Link>
                </li>
                <li>
                    <Link href="/">CONTACT</Link>
                </li>
            </ul>
            <div className='logos-header'>
            <Marcas imageUrl="/images/carrito.svg"></Marcas>
            <Marcas imageUrl="/images/lupa.svg"></Marcas>
            <Marcas imageUrl="/images/submenu.svg"></Marcas>
            </div>
        </div>
    )
}