export function Listas(props:{numero:string, tituloLista:string, flecha:any}){
    return(
        <div className="listas"> 
        <div className="listas-letras">
            <p>{props.numero}</p>
            <h1>{props.tituloLista}</h1>
        </div>
        <img src={props.flecha} alt="" />
        </div>
    )
}