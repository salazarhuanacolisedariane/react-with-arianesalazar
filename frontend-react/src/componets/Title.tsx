export default function Title(props:{Titulo:string, Parrafo:string}){
    return(
        <div className="texto"> 
            <h1>{props.Titulo}</h1>
            <p>{props.Parrafo}</p>
        </div>
    )
}