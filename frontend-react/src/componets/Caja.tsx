export function Caja(props:{ImgCaja:any, SubtituloCaja:string, ParrafoCaja:string}){
    return(
        <div className="nav-cajita">
            <img src={props.ImgCaja} alt="" />
            <div>
            <h4>{props.SubtituloCaja}</h4>
            <p>{props.ParrafoCaja}</p>
            </div>
        </div>
    )
}