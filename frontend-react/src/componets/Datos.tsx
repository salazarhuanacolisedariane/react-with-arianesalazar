export function Datos(props:{Numero:any, Description:string}){
    return(
        <div className="nav-dato"> 
            <h1>{props.Numero}</h1>
            <p>{props.Description}</p>
        </div>
    )
}