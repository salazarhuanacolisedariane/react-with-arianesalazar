export function TitleSubtitle(props:{titulo:string, subtitulo:string}){
    return(
        <div className="texto-dos"> 
            <h5>{props.subtitulo}</h5>
            <h1>{props.titulo}</h1>
        </div>
    )
}