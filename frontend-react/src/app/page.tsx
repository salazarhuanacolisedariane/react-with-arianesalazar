import * as React from 'react';
import Image from "next/image";
import styles from "./page.module.css";
import Title from "@/componets/Title";
import { Header } from "@/componets/Header";
import { Datos } from "@/componets/Datos";
import Marcas from "@/componets/Marcas";
import { Caja } from "@/componets/Caja";
import { TitleSubtitle } from "@/componets/TitleSubttitle";
import { Listas } from "@/componets/Listas";
import { Members } from "@/componets/Members";
import BButton from "@/componets/Button";
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';
import LocalPhoneIcon from '@mui/icons-material/LocalPhone';
import Link from '@mui/material/Link';


export default function Home() {
  const style = {
    py: 0,
    width: '100%',
    border: 'none',
    borderColor: 'divider',
    backgroundColor: '#FFFFFF26',
    
  };
  
  return (
    <main>
      <section className="container Contenedor-principal">
        <Header></Header>
        <div className="contenedor-header">
          <img src="/images/boton.svg" alt="" className="boton-header"/>
          <img src="/images/mani.svg" alt=""  className="mani-header"/>
          <img src="/images/adorno.svg" alt="" className="adorno-header"/>
        <div className="Contenedor-one">
            <Title Titulo="Digital Agency Solution" Parrafo="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."></Title>
        </div>
        <div className="Contenedor-two">
          <div className="parte-imagen">
            <img src="/images/img-one.jpg" alt="imagen-one" />
            <div className="caja1">
              <Caja ImgCaja={"/images/personas.svg"} SubtituloCaja="240 Business Peoples" ParrafoCaja="Already registered"></Caja>
            </div>
          </div>
          <div>
            <Datos Numero="12k+" Description="Project Complete"></Datos>
            <Datos Numero="7k+" Description="Happy Client"></Datos>
            <Datos Numero="10+" Description="Years Experience"></Datos>
            <Datos Numero="270+" Description="Win Awards"></Datos>
        </div>
        </div>
        </div>
        <div>
          <div className="icons-companys">
              <Marcas imageUrl="/images/compania1.svg"></Marcas>
              <Marcas imageUrl="/images/compania2.svg"></Marcas>
              <Marcas imageUrl="/images/compania3.svg"></Marcas>
              <Marcas imageUrl="/images/compania4.svg"></Marcas>
              <Marcas imageUrl="/images/compania5.svg"></Marcas>
              <Marcas imageUrl="/images/compania6.svg"></Marcas>
          </div>
        </div>
      </section>
      <section className="container Contenedor-principal-dos">
        <img src="/images/img-two-ii.svg" alt="" className="img-two-ii"/>
        <img src="/images/circulo-gris.svg" alt="" className="circulo-gris"/>
        <div>
          <div className="caja-uno">
            <div> <TitleSubtitle subtitulo="PORTFOLIO" titulo="We create places that connect, sustain & inspire"></TitleSubtitle></div>
            <div className="segunda-parte">
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
              <h4>VIEW ALL PORTFOLIO</h4>
            </div>
          </div>
          <div className="caja-dos-ii">
            <Listas numero="01." tituloLista="Web & Mobile Development" flecha={"/images/flecha-blanca.svg"}></Listas>
            <hr />
            <Listas numero="02." tituloLista="Interaction Design" flecha={"/images/flecha-cafe.svg"}></Listas>
            <hr />
            <Listas numero="03." tituloLista="Digital Marketing" flecha={"/images/flecha-blanca.svg"}></Listas>
            <hr />
            <Listas numero="04." tituloLista="Branding & Strategy" flecha={"/images/flecha-blanca.svg"}></Listas>
            <hr />
          </div>
        </div>
      </section>
      <section className="container Contenedor-principal-tres">
        <div className="caja-uno">
          <div>
          <TitleSubtitle subtitulo="OUR TEAM" titulo="Builds Our Future Experience Team Members"></TitleSubtitle>
          </div>
          <div className="segunda-parte">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore .</p>
            <div className="botones">
            <BButton imageBtn={"/images/flecha-izq.svg"}></BButton>
            <BButton imageBtn={"/images/flecha-der.svg"}></BButton>
            </div>
          </div>
        </div>
        <div className="caja-dos">
          <Members image={"/images/miembro-one.svg"} nombre="Aimee Calderon" ocupacion="Web Developer"></Members>
          <Members image={"/images/miembro-two.svg"} nombre="Raymond Horn" ocupacion="Digital Marketer"></Members>
          <Members image={"/images/miembro-three.svg"} nombre="Linda Towner" ocupacion="App Designer"></Members>
        </div>
        <div className="puntitos"><img src="/images/puntitos.svg" alt="" /></div>
      </section>
      <footer className="container Contenedor-principal-tres">
        <Box
          display="flex"
          alignItems="center"
          justifyContent="space-between"
          marginBottom="5%"
          marginTop="5%"
        >
          <Box
                height={200}
                width={300}
                my={2}
                gap={2}
                p={1}
          >
          <Typography variant="body1" gutterBottom className='texto-footer'>
          Lorem ipsum dolor sit amet consecte tur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua tepo the.
        </Typography>
        <Stack direction="row" spacing={1}>
          <Button variant="contained" startIcon={<LocalPhoneIcon />} sx={{backgroundColor: '#E3B27D'}} className='button-footer'>
          (973) 068 2300
          </Button>
        </Stack>
          </Box>
          <Box>
          <Box className="Quick">
          <Typography variant="h6"  fontWeight={600}>
          Quick Links
          </Typography>
          </Box>
          <Box
          sx={{display:"flex"}}
          >
          <Box 
                sx={{
                  display: { xs: 'none', sm: 'flex' },
                  flexDirection: 'column',
                  gap: 1,
                  minWidth: { xs: '100%', sm: '60%' },
                }}
                className="link-footer"
          >
          <Link sx={{ color: 'grey', textDecoration: 'none' }} href="#">
          About Us 
          </Link>
          <Link sx={{ color: 'grey', textDecoration: 'none' }}  href="#">
          Pricing Table 
          </Link>
          <Link sx={{ color: 'grey', textDecoration: 'none' }}  href="#">
          Contact Us  
          </Link>
          <Link sx={{ color: 'grey', textDecoration: 'none' }}  href="#">
          Meet Our Team
          </Link>
          <Link sx={{ color: 'grey', textDecoration: 'none' }}  href="#">
          Latest News
          </Link>
          </Box>
          <Box
                sx={{
                  display: { xs: 'none', sm: 'flex' },
                  flexDirection: 'column',
                  gap: 1,
                }}   
                className="link-footer"
          >
          <Link sx={{ color: 'grey', textDecoration: 'none' }}  href="#">
          Case Studies   
          </Link>
          <Link sx={{ color: 'grey', textDecoration: 'none' }}  href="#">
          FAQ’s
          </Link>
          <Link sx={{ color: 'grey', textDecoration: 'none' }}  href="#">
          Services
          </Link>
          <Link sx={{ color: 'grey', textDecoration: 'none' }}  href="#">
          Customer Support    
          </Link>
          </Box>
          </Box>
        </Box>
        </Box>
        <List sx={style}>
        <Divider variant="middle" component="li" />
        </List>
        <Box
          display="flex"
          alignItems="center"
          justifyContent="space-between"
          marginTop="20px"
        >
          <Box>
          <Typography variant="caption" display="block" sx={{ color: 'grey'}} gutterBottom>
            © 2023 davixq All Rights Reserved
          </Typography>
          </Box>
          <Box
          display="flex"
          alignItems="center"
          >
            <Box>
              <Typography variant="caption" display="block" sx={{ color: 'grey', marginRight:2}} gutterBottom>
              Terms of Use  
              </Typography>
            </Box>
            <Box>
              <Typography variant="caption" display="block" sx={{ color: 'grey'}} gutterBottom>
              Privacy Policy
              </Typography>
            </Box>

          </Box>
        </Box>
      </footer>
    </main>
  );
}
