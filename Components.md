***
# **_Práctica N°11_**

**Univ.:** Lised Ariane Salazar Huanaco 

**Docente:** Ing. Jose David Mamani Figueroa 

**Auxiliar:** Univ. Sergio Moises Apaza Caballero 

**Materia:** DISEÑO Y PROGRAMACIÓN GRÁFICA 

**Siglas:** SIS-313 G1 

**Fecha:** 23/05/2024
***

## **_TAREAS:_**
_**Identificar que componentes se usará a lo largo del proyecto y subirlos en un
archivo readme.md con las instrucciones dadas en la sección de NOTA(S).**_
## **🔆COMPONENTES**🔆
### TITLE: 
Este componente fue utilizado para el titulo de la pagina y tmabien contiene un parrafo 
### HEADER:
Este componete fue utlizado para el encabezado el cual contiene listas 
### CAJA:
Este componente fue utilizado para crear una pequeña cajita que contiene imagen, subtitulo y parrafo
### DATOS:
Este componente fue utilizado para crear pequeñas cajitas que contenias un numero 12K+ y un parrafo 
### MARCAS:
Este componente fue utilizado para el uzo de los logos de las empresar 
